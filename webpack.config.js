const path = require('path');

module.exports = {
    mode: "development",
    watch: true,
    entry:  './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "app.js",
    },

    module: {
        rules: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-react', '@babel/preset-env']
                }
            }],
        }]
    },
};