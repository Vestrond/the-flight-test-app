# API

#### GET /

Получить список рейсов.

**Response**:
```json
[{
  "number": "int",
  "from_city": "string",
  "to_city": "string",
  "real_time": "string",
  "approximate_time": "string",
  "aircraft_type": "string",
  "status": "string",
  "direction": "incoming|outgoing"
}]
```

Возможные статусы (поле _"status"_):

* Вылетел (_Take off_)
* Приземлился (_Landed_)
* Идет посадка (_Landing_)
* Задержан до <время> (_Delay XX:XX_)
* Потерпел крушение (_Crashed_)

#### PUT /{number}

Изменить рейс.

**Request**:
```json
{
  "number": "int",
  "from_city": "string",
  "to_city": "string",
  "real_time": "string",
  "approximate_time": "string",
  "aircraft_type": "string",
  "status": "string",
  "direction": "incoming|outgoing"
}
```

Response: **OK**

#### POST /

Добавить рейс.

```json
{
  "number": "int",
  "from_city": "string",
  "to_city": "string",
  "real_time": "string",
  "approximate_time": "string",
  "aircraft_type": "string",
  "status": "string",
  "direction": "incoming|outgoing"
}
```

Response: **OK**

#### DELETE /{number}

Удалить рейс.

Response: **OK**
