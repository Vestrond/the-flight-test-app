import {inside} from "./arrayHelper";

export function validateTime(time) {
    if (/[0-2][0-9]:[0-6][0-9]/.exec(time)) {
        let separatedTime = time.split(':');
        let hours = Number(separatedTime[0]);
        let minutes = Number(separatedTime[1]);

        if (hours >= 0 && hours <= 23 && minutes >= 0 && minutes <= 59) {
            return true;
        }
    }
    return false;
}

export function validateNumber(number) {
    return !isNaN(parseFloat(number)) && isFinite(number);
}

export function validateStatus(status) {
    status = status.toLowerCase();
    return (
        inside(status, ['take off', 'landed', 'landing', 'crashed'])
        || (status.startsWith('delay ') && validateTime(status.replace('delay ', '')))
    )
}