export function fillEnd(text, length, symbol = ' ') {
    while (text.length < length) {
        text += symbol;
    }

    return text;
}

export function fillStart(text, length, symbol = ' ') {
    while (text.length < length) {
        text = symbol + text;
    }

    return text;
}

export function camelToReadable(text) {
    for (let letter of text) {
        if (/[A-Z]/.exec(letter)) {
            text = text.replace(letter, ` ${letter.toLowerCase()}`)
        }
    }

    text = capitalize(text);
    return text;
}

export function capitalize(text) {
    return text.toLowerCase().replace(text[0], text[0].toUpperCase());
}