export function remove(number) {
    return this.filter(item => item.number !== number);
}

export function replace(number, ...replaceTo) {
    return this.map(function (item) {
        return item.number === number ? replaceTo[0] : item;
    })
}

export function clone(data) {
    return JSON.parse(JSON.stringify(data));
}

export function inside(element, array) {
    return array.indexOf(element) !== -1;
}