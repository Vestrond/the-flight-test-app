import {clone, remove, replace} from "./arrayHelper";

let fakeStore = [{
    approximateTime: "12:20",
    realTime: "14:05",
    number: "117",
    fromCity: "Moscow",
    toCity: "Pekin",
    aircraft: "Boeing 737",
    status: "Landed",
    direction: "incoming",
}];

function fakeRequest(method, url, data) {
    if (data) data = clone(data);

    if (method === 'GET') {
        return clone(fakeStore);
    }
    else if (method === 'POST') {
        fakeStore.push(data);
        return true;
    }
    else if (method === 'PUT') {
        fakeStore = fakeStore::replace(Number(url.replace('/', '')), data);
        return true;
    }
    else if (method === 'DELETE') {
        fakeStore = fakeStore::remove(Number(url.replace('/', '')));
        return true;
    }
}


function request(method, url, data) {
    return fakeRequest(method, url, data);

    // const xmlRequest = new XMLHttpRequest();
    //
    // xmlRequest.open(method, url);
    // data ? xmlRequest.send(data) : xmlRequest.send();
    //
    // if(xmlRequest.status !== 200){
    //     alert(`Http Request error:\n${xmlRequest.status}: ${xmlRequest.statusText}.`);
    // }
    // else {
    //     return JSON.parse(xmlRequest.responseText);
    // }
}

export function getRequest(url, data = undefined) {
    return request('GET', url, data);
}

export function postRequest(url, data = undefined) {
    return request('POST', url, data);
}

export function putRequest(url, data = undefined) {
    return request('PUT', url, data);
}

export function deleteRequest(url, data = undefined) {
    return request('DELETE', url, data);
}