export function filteredStory(story, filters) {
    let newStory = [];

    for(let elem of story){
        let shouldShowElement = true;

        for(let filterField of Object.keys(filters)){
            const filterVal = filters[filterField];

            if(filterVal && elem[filterField] !== filterVal){
                shouldShowElement = false;
            }
        }

        if(shouldShowElement){
            newStory.push(elem);
        }
    }

    return newStory;
}

export function getFieldVariables(store, fields) {
    let variants = new Set([]);

    for (let elem of store) {
        for (let field of fields) {
            variants.add(elem[field])
        }
    }

    return variants;
}