import React from 'react'
import {render} from 'react-dom'
import App from './components/App'
import {Provider} from "react-redux";
import flyStore from "./store/data";

const store = flyStore();

render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('render-element')
);