import React, {Component} from 'react'
import FlyTable from "./table/FlyTable";
import styled from 'styled-components'
import Footer from "./footer/Footer";
import {setData} from "../actions/data";
import {connect} from "react-redux";
import {getRequest} from "../utils/requestHelper";

const BoardWrapper = styled.div`
    display: block;
    height: 100%;
    width: 100%;
    background: #444;
`;

const Board = styled.div`
    display: inline-flex;
    width: 100%;
    height: 100%;
`;

class App extends Component {
    constructor(props) {
        super(props);

        // Get initial data from server
        let data = getRequest('/');
        if (data) {
            this.props.setData(data);
        }
    }

    render() {
        return (
            <BoardWrapper>
                <Board>
                    <FlyTable title="Incoming" direction="incoming"/>
                    <FlyTable title="Outgoing" direction="outgoing"/>
                </Board>
                <Footer/>
            </BoardWrapper>
        )
    }

}

function mapStateToProps(state) {
    return {
        flyData: state.flyData.elements,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setData: flyData => dispatch(setData(flyData)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);