import React, {Component} from 'react'
import {connect} from "react-redux";
import {camelToReadable} from "../../utils/textHelper";
import {getFieldVariables} from "../../utils/storeHelper"
import styled from 'styled-components'
import {updateFilters} from "../../actions/data";

const FilterWrapper = styled.div`
    font-family: monospace;
    display: inline-flex;
    margin: 0 20px;
    color: white;
    height: 100%;
`;

const SelectElement = styled.select`
    border: 1px solid gray;
    border-radius: 5px;
    background: transparent;
    color: white;
    font-family: monospace;
    line-height: 35px;
    height: 87%;
    margin: 2px;
`;

const Option = styled.option`
    background: #444;
    color: white;
`;

const Label = styled.label`
    line-height: 35px;
`;

class SelectableFilter extends Component {
    constructor(props) {
        super(props);

        this.fields = props.fields;
        this.state = {
            value: 'Nothing'
        }
    }

    render() {
        let readableFields = [];

        for (let field of this.fields) {
            readableFields.push(camelToReadable(field));
        }

        const options = [];

        for(let data of getFieldVariables(this.props.flyData, this.fields)){
            options.push(
                <Option key={data}>
                    {data}
                </Option>
            );
        }

        return (
            <FilterWrapper>
                <Label>{readableFields.join(', ')}:</Label>
                <SelectElement value={this.state.value} onChange={this.handleOnChange}>
                    <Option>Nothing</Option>
                    {options}
                </SelectElement>
            </FilterWrapper>
        )
    }

    handleOnChange = (e) => {
        const val = e.target.value;
        let filters = {};

        for (let field of this.fields) {
            filters[field] = val === 'Nothing' ? null : val;
        }
        this.props.updateFilters(filters);

        this.setState({
            value: val
        });
    }
}

function mapStateToProps(state) {
    return {
        flyData: state.flyData.elements,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateFilters: filters => dispatch(updateFilters(filters)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectableFilter);