import React, {Component} from 'react'
import styled from 'styled-components'
import SelectableFilter from "./SelectableFilter";
import {connect} from "react-redux";

const Label = styled.label`
    font-family: monospace;
    font-weight: bold;
    line-height: 35px;
    margin-left: 10px;
    color: #eee;
`;

const FooterBody = styled.div`
    position: fixed;
    display: ${props => props.isData ? "block": "none"};
    width: 100%;
    height: 35px;
    background: #333;
    bottom: 0;
`;

class Footer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        console.log(this.props.flyData);
        return (
            <FooterBody isData={this.props.flyData.length}>
                <Label>
                    Filters:
                </Label>
                <SelectableFilter fields={["fromCity"]}/>
                <SelectableFilter fields={["toCity"]}/>
                <SelectableFilter fields={["status"]}/>
            </FooterBody>
        )
    }
}

function mapStateToProps(state) {
    return {
        flyData: state.flyData.elements,
    }
}

export default connect(mapStateToProps)(Footer);