import React, {Component} from 'react'
import styled from 'styled-components'
import {CellButton, Row} from './TableRow'
import {camelToReadable, capitalize} from "../../utils/textHelper";
import {addData} from "../../actions/data";
import {connect} from "react-redux";
import {postRequest} from "../../utils/requestHelper";
import {validateNumber, validateStatus, validateTime} from "../../utils/validationHelper";

const AddButton = styled(CellButton)`
    text-align: center;
    
    &:hover {
        color: lightgreen;
    }
`;

const EditableCell = styled.input`
    color: white;
    background: #666;
    margin: 1px 3px;
    text-align: right;
    font-size: 16px;
    font-family: digital;
    border: none;
    width: ${props => props.width}px;
    
    &::placeholder {
        color: #AAA;
    }
`;

class AddRow extends Component {
    constructor(props) {
        super(props);

        this.defaultState = {
            isButton: true,
            approximateTime: '',
            realTime: '',
            number: '',
            fromCity: '',
            toCity: '',
            aircraft: '',
            status: '',
        };

        this.fieldPreSets = {
            approximateTime: {
                placeholder: '--:--',
                maxLength: 5,
                width: 44,
                validator: validateTime,
            },
            realTime: {
                placeholder: '--:--',
                maxLength: 5,
                width: 44,
                validator: validateTime,
            },
            number: {
                placeholder: '0000',
                maxLength: 4,
                width: 36,
                validator: validateNumber,
            },
            fromCity: {
                placeholder: 'City',
                maxLength: 13,
                width: 115,
                validator: null,
            },
            toCity: {
                placeholder: 'City',
                maxLength: 13,
                width: 114,
                validator: null,
            },
            aircraft: {
                placeholder: 'Aircraft',
                maxLength: 13,
                width: 115,
                validator: null,
            },
            status: {
                placeholder: 'Status',
                maxLength: 11,
                width: 97,
                validator: validateStatus,
            },
        };

        this.state = this.defaultState;
        this.direction = props.direction;
    }

    render() {
        if (this.state.isButton) {
            return (
                <AddButton onClick={this.handleOnButtonClick}>
                    Add new
                </AddButton>
            );
        }
        else {
            let cells = [];

            for (let field of Object.keys(this.fieldPreSets)) {
                cells.push(
                    <EditableCell name={field} key={field} type="text"
                                  onChange={this.handleOnCellChange} onBlur={this.handleOnBlurCell}
                                  placeholder={this.fieldPreSets[field].placeholder}
                                  maxLength={this.fieldPreSets[field].maxLength}
                                  width={this.fieldPreSets[field].width}
                                  value={this.state[field]}/>
                );
            }

            return (
                <div>
                    <Row>
                        {cells}
                        <CellButton onClick={this.handleOnCancelButtonClick}>X</CellButton>
                    </Row>
                    <AddButton onClick={this.handleOnAcceptButtonClick}>
                        Accept
                    </AddButton>
                </div>
            );
        }
    }

    handleOnCellChange = (e) => {
        const val = e.target.value;
        const field = e.target.name;
        const validator = this.fieldPreSets[field].validator;

        if (validator && validator(val)) {
            // Return white font if value is ok.
            e.target.style.color = "white";
        }

        this.setState({[field]: val});
    };

    handleOnBlurCell = (e) => {
        const val = e.target.value;
        const validator = this.fieldPreSets[e.target.name].validator;

        if (validator) {
            e.target.style.color = validator(val) ? "white" : "#f88";
        }
    };

    handleOnButtonClick = (e) => {
        this.setState({isButton: false});
    };

    handleOnAcceptButtonClick = (e) => {
        for (let key of Object.keys(this.state)) {
            if (key === 'isButton') continue;

            // Check that each requirement field is filled.
            if (key !== 'realTime' && !this.state[key]) {
                e.target.textContent = `${camelToReadable(key)} is empty!`;
                return
            }

            // ... And valid
            if (key !== 'isButton' && this.state[key]) {
                const validator = this.fieldPreSets[key].validator;
                if (validator && !validator(this.state[key])) {
                    e.target.textContent = `${camelToReadable(key)} is not valid!`;
                    return
                }
            }
        }

        // Check that this flight is unique
        for (let element of this.props.flyData) {
            if (this.state.number === element.number) {
                e.target.textContent = `A flight with this number is already exists!`;
                return
            }
        }

        // Success!
        let data = {
            approximateTime: this.state.approximateTime,
            realTime: this.state.realTime,
            number: this.state.number,
            fromCity: this.state.fromCity,
            toCity: this.state.toCity,
            aircraft: this.state.aircraft,
            status: capitalize(this.state.status),
            direction: this.direction,
        };

        if (postRequest('/', data)) {
            this.props.addData(data);
            this.setState(this.defaultState);
        }
    };

    handleOnCancelButtonClick = (e) => {
        this.setState({isButton: true});
    };
}

function mapStateToProps(state) {
    return {
        flyData: state.flyData.elements,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addData: flyData => dispatch(addData(flyData)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddRow);