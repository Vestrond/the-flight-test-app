import React, {Component} from 'react'
import styled from 'styled-components'
import {fillStart} from "../../utils/textHelper";
import {removeData, replaceData} from "../../actions/data";
import {connect} from "react-redux";
import {deleteRequest, putRequest} from "../../utils/requestHelper";
import {validateNumber, validateStatus, validateTime} from "../../utils/validationHelper";

export const Row = styled.div`
    display: inline-flex;
    width: 100%;
`;

export const Cell = styled.pre`
    color: yellow;
    background: #666;
    margin: 1px 3px;
    font-size: 16px;
    font-family: digital;
`;

export const CellButton = styled(Cell)`
    cursor: pointer;
    user-select: none;
    
    &:hover {
        color: red;
    }
    
    &:active {
        color: white;
    }
    
`;

const EditableCell = styled.input`
    color: white;
    background: #666;
    margin: 1px 3px;
    text-align: right;
    font-size: 16px;
    font-family: digital;
    border: none;
    width: ${props => props.width}px;
    
    &::placeholder {
        color: #AAA;
    }
`;

export const timeSize = 5;
export const numberSize = 4;
export const citySize = 13;
export const aircraftTypeSize = 13;
export const statusSize = 11;

class TableRowComponent extends Component {
    constructor(props) {
        super(props);

        this.fieldNames = {
            "approximateTime": {maxLength: timeSize, validator: validateTime},
            "realTime": {maxLength: timeSize, validator: validateTime},
            "number": {maxLength: numberSize, validator: validateNumber},
            "fromCity": {maxLength: citySize, validator: null},
            "toCity": {maxLength: citySize, validator: null},
            "aircraft": {maxLength: aircraftTypeSize, validator: null},
            "status": {maxLength: statusSize, validator: validateStatus},
        };
        this.state = {
            number: props.number,
            editableCell: null,
            editableCellValue: null,
            editableCellWidth: null,
        };
    }

    render() {
        let cells = [];

        for (let field of Object.keys(this.fieldNames)) {
            if (this.state.editableCell === field) {
                cells.push(
                    <EditableCell name={field} type="text" maxLength={this.fieldNames[field].maxLength} key={field}
                                  autoFocus
                                  width={this.state.editableCellWidth}
                                  value={this.state.editableCellValue}
                                  onChange={this.handleCellEdited}
                                  onBlur={this.handleEditableCellOnBlur}/>
                )
            }
            else {
                cells.push(
                    <Cell name={field} onDoubleClick={this.handleCellDoubleClick} key={field}>
                        {fillStart(this.data()[field], this.fieldNames[field].maxLength)}
                    </Cell>
                )
            }
        }

        return (
            <Row>
                {cells}
                <CellButton onClick={this.handleInRemoveButtonClick}>X</CellButton>
            </Row>
        )
    }

    data = () => {
        for (let data of this.props.flyData) {
            if (data.number === this.props.number) {
                return data;
            }
        }
    };

    handleEditableCellOnBlur = (e) => {
        // Check that value is valid
        const name = e.target.attributes.name.value;
        const validator = this.fieldNames[name].validator;

        if(validator && !validator(e.target.value)){
            e.target.style.color = '#f88';
            return;
        }

        let fields = {};

        for (let field of Object.keys(this.fieldNames)) {
            fields[field] = field === name ? e.target.value : this.data()[field];
        }

        if (name === 'number') {
            this.setState({
                number: e.target.value
            });
        }

        const data = {
            ...fields,
            direction: this.data().direction,
        };

        if (putRequest(`/${this.data().number}`, data)) {
            this.props.replaceData(this.data().number, data);
            this.setState({
                editableCell: null,
                editableCellValue: null,
                editableCellWidth: null,
            });
        }
    };

    handleCellEdited = (e) => {
        this.setState({editableCellValue: e.target.value});
    };

    handleCellDoubleClick = (e) => {
        const field = e.target.attributes.name.value;

        this.setState({
            editableCell: field,
            editableCellValue: this.data()[field],
            editableCellWidth: e.target.clientWidth,
        });
    };

    handleInRemoveButtonClick = (e) => {
        const number = this.data().number;

        if (deleteRequest(`/${number}`)) {
            this.props.removeData({
                number: number,
            });
        }

    }
}

function mapStateToProps(state) {
    return {
        flyData: state.flyData.elements,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        removeData: flyData => dispatch(removeData(flyData)),
        replaceData: (number, flyData) => dispatch(replaceData(number, flyData)),
    }
}

export const TableRow = connect(mapStateToProps, mapDispatchToProps)(TableRowComponent);