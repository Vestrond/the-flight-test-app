import React, {Component} from 'react'
import {aircraftTypeSize, citySize, numberSize, statusSize, TableRow} from "./TableRow";
import styled from 'styled-components'
import AddRow from "./AddRow";
import {connect} from "react-redux";
import {filteredStory} from "../../utils/storeHelper";

const Table = styled.div`
    width: 622px;
    height: 100vh;
    font-family: monospace;
    margin 0 auto;
    display: block;
`;

const StaticText = styled.div`
    color: #eee;
    text-transform: uppercase;
    text-align: center;
`;

const Title = styled(StaticText)`
    font-size: 24px;
`;

const ColumnHint = styled(StaticText)`
    text-align: left;
    display: block;
    height: 32px;
`;

const Cell = styled.span`
    font-size: 10px;
    height: 32px;
    padding-left: 3px;
    padding-right: 3px;
    text-align: center;
    display: table-cell;
    vertical-align: middle;
    width: ${props => props.width}px;
`;

class FlyTable extends Component {
    constructor(props) {
        super(props);

        this.direction = props.direction;
        this.title = props.title;
        this.sizeMultiple = 9;
    }

    render() {
        let rows = [];

        for (let data of filteredStory(this.props.flyData, this.props.filters)) {
            if (this.direction === data.direction) {
                rows.push(<TableRow number={data.number} key={data.number}/>)
            }
        }

        return (
            <Table className="fly-table">
                <Title>{this.title}</Title>
                <ColumnHint>
                    <Cell width={97}>
                        Time<br/>
                        Approx Real
                    </Cell>
                    <Cell width={numberSize * this.sizeMultiple}>№</Cell>
                    <Cell width={citySize * this.sizeMultiple}>From</Cell>
                    <Cell width={citySize * this.sizeMultiple}>To</Cell>
                    <Cell width={aircraftTypeSize * this.sizeMultiple}>Aircraft</Cell>
                    <Cell width={statusSize * this.sizeMultiple}>Status</Cell>
                </ColumnHint>
                {rows}
                <AddRow direction={this.direction}/>
            </Table>
        )
    }
}

function mapStateToProps(state) {
    return {
        flyData: state.flyData.elements,
        filters: state.flyData.filters,
    }
}

export default connect(mapStateToProps)(FlyTable);