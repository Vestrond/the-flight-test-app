import {ADD_DATA, REMOVE_DATA, REPLACE_DATA, SET_DATA, UPDATE_FILTERS} from "../actions/data";
import {clone, remove, replace} from "../utils/arrayHelper";

const initialState = {
    elements: [],
    filters: {},
};

export default function (state = initialState, action) {
    console.log('Reducer was called by', action, 'action. Current:', state);

    const data = state.elements.slice();
    const filters = clone(state.filters);

    switch (action.type) {
        case SET_DATA:
            return {
                ...state,
                elements: action.flyData,
            };
        case ADD_DATA:
            // Check that element is unique
            for (let element of data){
                if (action.flyData.number === element.number){
                    console.log(`Рейс с таким номером уже существует.`);
                    return state;
                }
            }

            data.push(action.flyData);
            return {
                ...state,
                elements: data,
            };
        case REMOVE_DATA:
            return {
                ...state,
                elements: data::remove(action.number)
            };
        case REPLACE_DATA:
            return {
                ...state,
                elements: data::replace(action.number, action.flyData)
            };
        case UPDATE_FILTERS:
            return {
                ...state,
                filters: {
                    ...filters,
                    ...action.filters,
                },
            };
        default:
            return state;
    }
}