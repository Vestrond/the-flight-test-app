import flyData from './flyData'
import {combineReducers} from "redux";

export default combineReducers({
    flyData: flyData,
})