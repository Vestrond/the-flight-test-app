export const SET_DATA = 'SET_DATA';
export const ADD_DATA = 'ADD_DATA';
export const REMOVE_DATA = 'REMOVE_DATA';
export const REPLACE_DATA = 'REPLACE_DATA';
export const UPDATE_FILTERS = 'UPDATE_FILTERS';

export const setData = (flyData) => ({
    type: SET_DATA,
    flyData: flyData,
});

export const addData = (flyData) => ({
    type: ADD_DATA,
    flyData: flyData,
});

export const removeData = (data) => ({
    type: REMOVE_DATA,
    number: data.number,
});

export const replaceData = (number, newFlyData) => ({
    type: REPLACE_DATA,
    number: number,
    flyData: newFlyData,
});

export const updateFilters = (filters) => ({
    type: UPDATE_FILTERS,
    filters: filters,
});